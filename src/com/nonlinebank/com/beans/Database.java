package com.nonlinebank.com.beans;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    public static Connection connect() {
        String url = "jdbc:mysql://localhost:3306/onlinebank";
        String utilisateur = "root";
        String motDePasse = "123456";

        Connection connexion = null;

        try {
            connexion = DriverManager.getConnection(url, utilisateur, motDePasse);
            return connexion;
        } catch (SQLException e) {
            System.err.println(e);


        }
        return null;
    }
}

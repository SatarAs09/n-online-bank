package com.nonlinebank.com.beans;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


import com.nonlinebank.com.beans.Database;
import com.nonlinebank.com.beans.Clients;
import com.nonlinebank.com.beans.Database;
import com.sun.security.ntlm.Client;

import javax.servlet.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import com.nonlinebank.com.beans.Clients;

public class Clients {
    private int id;

    private String nom;
    private String prenom;
    private Date dateDeNaissance;
    private Boolean contacts;
    private String email;
    private String telephone;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;

    }

    public static List selectClients(String id) {

        Connection connection = Database.connect();
        try{
            Statement statement = connection.createStatement();
            ResultSet data = statement.executeQuery("SELECT * FROM clients WHERE id = '" + id + "'");
            List<Clients> results = new ArrayList<>();

            while(data.next()) {
                Clients clients1 = new Clients();

                clients1.setId(data.getInt("id"));
                clients1.setNom(data.getString("nom"));
                clients1.setPrenom(data.getString("prenom"));
                clients1.setEmail(data.getString("email"));
                clients1.setTelephone(data.getString("telephone"));

                results.add(clients1);
            }

            return results;
        } catch (Exception e) {
            System.err.println(e);
        }

        return null;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateDeNaissance() {
        return dateDeNaissance;


    }

    public void setDateDeNaissance(Date dateDeNaissance) {
        this.dateDeNaissance = dateDeNaissance;
    }

    public Boolean getContacts() {
        return contacts;
    }

    public void setContacts(Boolean contacts) {
        this.contacts = contacts;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }





}


package com.nonlinebank.com.beans;

public class produit {
    private int idproduits;
    private String label;
    private Boolean exclusif;

    public int getIdproduits() {
        return idproduits;
    }

    public void setIdproduits(int idproduits) {
        this.idproduits = idproduits;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Boolean getExclusif() {
        return exclusif;
    }

    public void setExclusif(Boolean exclusif) {
        this.exclusif = exclusif;
    }
}

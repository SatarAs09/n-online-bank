package com.nonlinebank.com.beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


import com.nonlinebank.com.beans.Database;
import com.nonlinebank.com.beans.Clients;
import com.nonlinebank.com.beans.Database;
import com.sun.security.ntlm.Client;

import javax.servlet.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import com.nonlinebank.com.beans.Clients;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

public class CompteClient {

    private int idrevenus;
    private float montant;
    private Date date;
    private int idClient;

    public static List selectCompte(String id) {

        Connection connection = Database.connect();
        try{
            Statement statement = connection.createStatement();
            ResultSet data = statement.executeQuery("SELECT * FROM revenus WHERE idClient = "+id);
            List<CompteClient> results = new ArrayList<>();
            while(data.next()) {
                CompteClient compte = new CompteClient();

                compte.setIdrevenus(data.getInt("idrevenus"));
                compte.setMontant(data.getFloat("montant"));



                results.add(compte);
            }

            return results;
        } catch (Exception e) {
            System.err.println(e);
        }

        return null;
    }


    public int getIdrevenus() {
        return idrevenus;
    }

    public void setIdrevenus(int idrevenus) {
        this.idrevenus = idrevenus;
    }

    public float getMontant() {
        return montant;
    }

    public void setMontant(float montant) {
        this.montant = montant;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }



    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }


}

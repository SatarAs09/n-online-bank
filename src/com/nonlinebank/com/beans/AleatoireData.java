package com.nonlinebank.com.beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class AleatoireData {

        public static String getNom() {

            List nom = new ArrayList();
            nom.add("Dubois");
            nom.add("Bazin");
            nom.add("Audoin");
            nom.add("Lamain");
            nom.add("Duval");
            nom.add("Langlois");
            nom.add("Morsin");
            nom.add("Santino");
            nom.add("Martin");
            nom.add("Benard");
            nom.add("Petit");
            nom.add("Durand");
            nom.add("Duponc");
            nom.add("Lechevalier");
            nom.add("Lacroix");
            nom.add("Leroy");
            nom.add("Dumont");
            nom.add("Moroi");
            nom.add("Revert");
            nom.add("Lambert");


            int nomSize = nom.size() - 1;
            Random rand = new Random();


            int i = randomBetween(0, nomSize);

            String nomClients = (String) nom.get(i);

            return nomClients;


        }
        public static String getPrenom(){
            List prenom = new ArrayList();
            prenom.add("Arthur");
            prenom.add("Sebastien");
            prenom.add("Aurelie");
            prenom.add("Juliette");
            prenom.add("Adrien");
            prenom.add("Alfredo");
            prenom.add("Hugo");
            prenom.add("Emilie");
            prenom.add("Manon");
            prenom.add("Andrea");
            prenom.add("Mathilde");
            prenom.add("Clarence");
            prenom.add("Lucien");
            prenom.add("Louis");
            prenom.add("Anais");
            prenom.add("Michel");
            prenom.add("Romain");
            prenom.add("Clément");
            prenom.add("Joffrey");
            prenom.add("Jean");

            int prenomSize = prenom.size()-1;
            Random random = new Random();

            int j = randomBetween(0,prenomSize);
            String prenomClients = (String)prenom.get(j);

            return prenomClients;
        }


    public static java.sql.Date getdateDeNaissance(){
        GregorianCalendar calendar = new GregorianCalendar();
        Random random = new Random();
        int year = randomBetween(1950,2000);
        calendar.set(calendar.YEAR,year);

        List day = new ArrayList();
        day.add("01");
        day.add("02");
        day.add("03");
        day.add("04");
        day.add("05");
        day.add("06");
        day.add("07");
        day.add("08");
        day.add("09");
        day.add("10");
        day.add("11");
        day.add("12");
        day.add("13");
        day.add("14");
        day.add("15");
        day.add("16");
        day.add("17");
        day.add("18");
        day.add("19");
        day.add("20");
        day.add("21");
        day.add("22");
        day.add("23");
        day.add("24");
        day.add("25");
        day.add("26");
        day.add("27");
        day.add("28");

        int daySize = day.size()-1;
        Random random1 = new Random();
        int dayRandom = random1.nextInt(daySize-0+1);

        String dayBirth = (String) day.get(dayRandom);

        int dayBirth2 = Integer.parseInt(dayBirth);




        calendar.set(calendar.DAY_OF_MONTH,dayBirth2);

        int month = randomBetween(1,12);
        calendar.set(calendar.MONTH,month);

        String  birthday = calendar.get(calendar.YEAR)+"-"+calendar.get(calendar.MONTH)+"-"+calendar.get(calendar.DAY_OF_MONTH);

        try{

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date = format.parse(birthday);
            DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            String date2 = format1.format(date);

            Date parsed = format1.parse(date2);
            java.sql.Date sql = new java.sql.Date(parsed.getTime());

            return sql;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;



    }

    public static int randomBetween(int start,int end){
        return start+(int)Math.round(Math.random()*(end-start));
    }

    public static String getTelephone(){
        List<String> number = new ArrayList<>();

        for(int i =0;i<99;i++){
            String j = String.valueOf(i);
            number.add(j);
        }
        int numberSize = number.size()-1;
        Random random = new Random();
        int random1 = random.nextInt(numberSize-0+1);
        String one = number.get(random1);

        int oneInt = Integer.parseInt(one);
        if(oneInt<10){
            one="0"+number.get(random1);
        }

        Random random2 = new Random();
        int random3 = random2.nextInt(numberSize-0+1);
        String two = number.get(random3);
        int twoInt = Integer.parseInt(two);
        if(twoInt<10){
            two="0"+number.get(random3);
        }

        Random random4 = new Random();
        int random5 = random4.nextInt(numberSize-0+1);
        String three = number.get(random5);
        int threeInt = Integer.parseInt(three);
        if(threeInt<10){
            three="0"+number.get(random5);
        }

        Random random6 = new Random();
        int random7 = random6.nextInt(numberSize-0+1);
        String four = number.get(random7);
        int fourInt = Integer.parseInt(four);
        if(fourInt<10){
            four="0"+number.get(random7);
        }

        String numberPhone = "06."+one+"."+two+"."+three+"."+four;

        return numberPhone;
    }

    public static Boolean getContacts(){
        Boolean contact = Math.random()<0.5;

        return contact;
    }





}
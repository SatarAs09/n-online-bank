package com.nonlinebank.com.servlets;

import com.nonlinebank.com.beans.AleatoireData;
import com.nonlinebank.com.beans.Database;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

@WebServlet(name = "GenerateClients",
            urlPatterns = "/Aleatoire")
public class GenerateClients extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            Connection connection = Database.connect();
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO clients(id,nom,prenom,dateDeNaissance,contacts,email,telephone,idSituationFamiliale,idSituationPro) VALUES (NULL ,?,?,?,?,?,?,NULL ,NULL )");
            String nom = AleatoireData.getNom();
            String prenom = AleatoireData.getPrenom();
            String email = prenom + "." + nom + "@example.com";
            preparedStatement.setString(1, nom);
            preparedStatement.setString(2, prenom);
            preparedStatement.setDate(3, AleatoireData.getdateDeNaissance());
            preparedStatement.setBoolean(4, AleatoireData.getContacts());
            preparedStatement.setString(5, email);
            preparedStatement.setString(6, AleatoireData.getTelephone());


            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Servlet B spotted");

        response.sendRedirect("liste");

    }




    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher view = request.getRequestDispatcher("generateClient.jsp");
        view.forward(request, response);


    }
}

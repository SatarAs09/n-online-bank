package com.nonlinebank.com.servlets;

import com.nonlinebank.com.beans.AleatoireData;
import com.nonlinebank.com.beans.Database;
import com.nonlinebank.com.beans.GenerateCompte;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

@WebServlet(name = "GenAcountServlet",
            urlPatterns = "/Revenus")
public class GenAcountServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Float amount = GenerateCompte.montant();
        Date date = GenerateCompte.Date();
        int type = GenerateCompte.generateRevenusId();
        int id = GenerateCompte.idClient();
        int Id = GenerateCompte.idTypeMontant();


        Connection connection = Database.connect();
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO revenus(montant,date,idTypeMontant,idClient) VALUES (?,?,?,? )");
            statement.setFloat(1,amount);
            statement.setDate(2,date);
            statement.setInt(3,type);
            statement.setInt(4, Id);



            statement.executeUpdate();

        }catch (Exception e){
            e.printStackTrace();
        }

        response.sendRedirect("/");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher view = request.getRequestDispatcher("formCompte.jsp");
        view.forward(request, response);

    }
}

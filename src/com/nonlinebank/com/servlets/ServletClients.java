package com.nonlinebank.com.servlets;

import com.nonlinebank.com.beans.Database;
import com.nonlinebank.com.beans.Clients;
import com.nonlinebank.com.beans.Database;
import com.sun.security.ntlm.Client;

import javax.servlet.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import com.nonlinebank.com.beans.Clients;



@WebServlet(name = "Servlet",
        urlPatterns = "/showClients")
public class ServletClients extends javax.servlet.http.HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        // System.out.println("Servlet spotted");
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = Database.connect();
            Statement statement = connection.createStatement();
            ResultSet data = statement.executeQuery("SELECT * FROM clients");
            List<Clients> result = new ArrayList<>();

            while(data.next()){

                Clients clients = new Clients();

                clients.setId(data.getInt("id"));
                clients.setNom(data.getString("nom"));
                clients.setPrenom(data.getString("prenom"));
                clients.setDateDeNaissance(data.getDate("dateDeNaissance"));
                clients.setContacts(data.getBoolean("contacts"));
                clients.setEmail(data.getString("email"));
                clients.setTelephone(data.getString("telephone"));

                result.add(clients);

            }
            request.setAttribute("result",result);

        }catch (Exception e){
            e.printStackTrace();
        }
        RequestDispatcher View = request.getRequestDispatcher("showClients.jsp");
        View.forward(request,response);
    }
}

package com.nonlinebank.com.servlets;
import com.nonlinebank.com.beans.CompteClient;
import com.nonlinebank.com.beans.Database;
import com.nonlinebank.com.beans.Clients;
import com.sun.security.ntlm.Client;
import javax.servlet.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import com.nonlinebank.com.beans.Clients;


@WebServlet(name = "CompteServlet",
        urlPatterns = "/CompteServlet")
public class CompteServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            String id = request.getParameter("id");

            List result = Clients.selectClients(id);
            String iduser = request.getParameter("id");

            List results = CompteClient.selectCompte(iduser);

            request.setAttribute("comptes", results);


        }catch (Exception e) {
            e.printStackTrace();
        }
        RequestDispatcher View = request.getRequestDispatcher("Comptes/compteClient.jsp");
        View.forward(request,response);
    }


}

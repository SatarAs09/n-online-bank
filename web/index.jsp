<%@ page import="java.util.List" %>
<%@ page import="com.nonlinebank.com.beans.Clients" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Accueil</title>
    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" type="image/png" href="images/favicon.png">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
</head>
<body>
<div class="wrapper">
    <!-- Sidebar -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3>N'Online Bank</h3>
            <strong>OB</strong>
        </div>
        <ul class="list-unstyled components">
            <li>
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-home"></i>
                    Accueil
                </a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li>
                        <a href="#">Tableau de bord</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#compteSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-briefcase"></i>
                    Comptes
                </a>
                <ul class="collapse list-unstyled" id="compteSubmenu">
                    <li>
                        <a href="#compteTypeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Gérer les comptes</a>
                        <ul class="collapse list-unstyled" id="compteTypeSubmenu">
                            <li>
                                <a href="#">&nbsp; Compte courant</a>
                            </li>
                            <li>
                                <a href="#">&nbsp; Livret A</a>
                            </li>
                            <li>
                                <a href="#">&nbsp; Livret jeune</a>
                            </li>
                            <hr class="solid">
                        </ul>
                    </li>
                    <li>
                        <a href="#">Opérations</a>
                    </li>
                    <li>
                        <a href="#">Détails</a>
                    </li>
                    <li>
                        <a href="#">Souscriptions</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#creditSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-copy"></i>
                    Crédits
                </a>
                <ul class="collapse list-unstyled" id="creditSubmenu">
                    <li>
                        <a href="#">Consommation</a>
                    </li>
                    <li>
                        <a href="#">Immobilier</a>
                    </li>
                    <li>
                        <a href="#">Automobile</a>
                    </li>
                    <li>
                        <a href="#">Etudiant</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#epargneSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-image"></i>
                    Epargne
                </a>
                <ul class="collapse list-unstyled" id="epargneSubmenu">
                    <li>
                        <a href="#">Plan logement</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#assuranceSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-paper-plane"></i>
                    Assurances
                </a>
                <ul class="collapse list-unstyled" id="assuranceSubmenu">
                    <li>
                        <a href="#">Assurance vie</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-question"></i>
                    FAQ
                </a>
            </li>
                <li class="nav-item">
                    <a class="nav-link" href="LogoutController">Déconnexion</a>
                </li>
        </ul>
    </nav>
    <!-- Page Content -->
    <div id="content">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-bars"></i>
                </button>
                <li class="nav-item list-unstyled">
                    <a class="nav-link" href="#" style="color: #17A2B8;"><i class="fas fa-search"></i>&nbsp; Recherche</a>
                </li>
                </button>
                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>
                <nav class="collapse navbar-collapse bg-light" aria-label="breadcrumb" id="navbarSupportedContent">
                    <ul class="breadcrumb ml-auto mb-auto text-info">
                        <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tableau de bord</li>
                    </ul>
                </nav>

                <!-- <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Page</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Page</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Page</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Page</a>
                        </li>
                    </ul>
                </div> -->
            </div>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success alert-dismissable">

                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                            ×
                        </button>
                        <h4>
                            Bienvenue sur le tableau de bord !
                        </h4> <strong>Attention!</strong> Sur ce compte, vous avez accès à des informations sensibles.
                    </div>
                    <div class="page-header">
                        <h1>
                            Tableau de bord
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <h2>Section title</h2>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>

                    <th>#</th>
                    <th>ID</th>
                    <th>Nom</th>
                    <th>Prenom</th>
                    <th>Page Client</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <%
                        //List<Clients> clients =(List<Clients>) request.getAttribute("result");
                        //for (Clients clients1:clients){
                    %>
                    <td><%//out.println(clients1.getId());%></td>
                    <td><%//out.println(clients1.getNom());%> </td>
                    <td><%//out.println(clients1.getPrenom());%></td>
                </tr>

                <% //} %>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- jQuery CDN - Slim version (=without AJAX) -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!-- Popper.JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<!-- Bootstrap JS -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });
</script>
</body>
</html>

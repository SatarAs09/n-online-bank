<%--
  Created by IntelliJ IDEA.
  User: cleme
  Date: 10/12/2018
  Time: 12:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Connexion</title>

    <link rel="stylesheet" href="./css/style.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="MDB/css/bootstrap.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="MDB/css/mdb.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="MDB/css/style.css" rel="stylesheet">
</head>

<body>
<div class="text-center">
    <span class="spacen titre">Online Bank</span>
</div>
<!-- Material form login -->
<div class ="spacer2 container">
    <div class="card">

        <h5 class="card-header info-color white-text text-center py-4">
            <strong>Se connecter</strong>
        </h5>

        <!--Card content-->
        <div class="card-body px-lg-5 pt-0">

            <!-- Form -->
            <form action="LoginController" method="post" class="text-center" style="color: #757575;">

                <!-- Email -->
                <div class="md-form">
                    <input type="text" name="nom" id="materialLoginFormEmail" class="form-control" placeholder="Nom">

                </div>

                <!-- Password -->
                <div class="md-form">
                    <input type="password" name="mdp" id="materialLoginFormPassword" class="form-control" placeholder="Mot de passe">

                </div>

                <div class="text-center">

                    <input type="submit" class="btn btn-success" value="Connexion">

                </div>


            </form>
            <!-- Form -->

        </div>

    </div>

</div>
<!-- Material form login -->

</body>
</html>

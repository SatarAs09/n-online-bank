<%@ page import="java.util.List" %>
<%@ page import="com.nonlinebank.com.beans.Clients" %>
<%@ page import="com.nonlinebank.com.beans.CompteClient" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Infos Client</title>
    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="./css/style.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
</head>
<body>



<div class="wrapper">
    <!-- Sidebar -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3>N'Online Bank</h3>
            <strong>OB</strong>
        </div>
        <ul class="list-unstyled components">
            <li>
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-home"></i>
                    Accueil
                </a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li>
                        <a href="#">Tableau de bord</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#compteSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-briefcase"></i>
                    Comptes
                </a>
                <ul class="collapse list-unstyled" id="compteSubmenu">
                    <li>
                        <a href="#compteTypeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Gérer les comptes</a>
                        <ul class="collapse list-unstyled" id="compteTypeSubmenu">
                            <li>
                                <a href="#">&nbsp; Compte courant</a>
                            </li>
                            <li>
                                <a href="#">&nbsp; Livret A</a>
                            </li>
                            <li>
                                <a href="#">&nbsp; Livret jeune</a>
                            </li>
                            <hr class="solid">
                        </ul>
                    </li>
                    <li>
                        <a href="#">Opérations</a>
                    </li>
                    <li>
                        <a href="#">Détails</a>
                    </li>
                    <li>
                        <a href="#">Souscriptions</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#creditSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-copy"></i>
                    Crédits
                </a>
                <ul class="collapse list-unstyled" id="creditSubmenu">
                    <li>
                        <a href="#">Consommation</a>
                    </li>
                    <li>
                        <a href="#">Immobilier</a>
                    </li>
                    <li>
                        <a href="#">Automobile</a>
                    </li>
                    <li>
                        <a href="#">Etudiant</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#epargneSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-image"></i>
                    Epargne
                </a>
                <ul class="collapse list-unstyled" id="epargneSubmenu">
                    <li>
                        <a href="#">Plan logement</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#assuranceSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-paper-plane"></i>
                    Assurances
                </a>
                <ul class="collapse list-unstyled" id="assuranceSubmenu">
                    <li>
                        <a href="#">Assurance vie</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-question"></i>
                    FAQ
                </a>
            </li>
            <%--<div class="collapse navbar-collapse">--%>
            <%--<ul class="nav navbar-nav ml-auto">--%>
            <%--<li class="nav-item">--%>
            <%--<a class="nav-link" href="#">Déconnexion</a>--%>
            <%--</li>--%>
            <%--</ul>--%>
            <%--</div>--%>
        </ul>
    </nav>



    <div id="content">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-bars"></i>
                </button>
                <ul>
                    <li class="nav-item list-unstyled">
                        <a class="nav-link" href="#" style="color: #17A2B8;"><i class="fas fa-search"></i>&nbsp; Recherche</a>
                    </li>
                </ul>
                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>
                <nav class="collapse navbar-collapse bg-light" aria-label="breadcrumb" id="navbarSupportedContent">
                    <ul class="breadcrumb ml-auto mb-auto text-info">
                        <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tableau de bord</li>
                    </ul>
                </nav>


            </div>
        </nav>

        <div class="container">
            <%  List<Clients> clients = (List<Clients>) request.getAttribute("clients");

                for (Clients clients1 : clients ) {

            %>

            <div class="container">
                <label>Client : <% out.println(clients1.getNom() + " " + clients1.getPrenom()); %> </label>

                <p>Numéro client : 15862545</p>
            </div>


            <div class="container infosClient">
                <div class="row">

                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="text-center">
                                    <h2>Informations Client</h2>
                                </div>
                                <hr>
                            </div>
                        </div>



                        <div class="spacer row">
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Nom : <% out.println(clients1.getNom()); %></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Prénom : <% out.println(clients1.getPrenom()); %></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Email : <% out.println(clients1.getEmail()); %></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Télephone : <% out.println(clients1.getTelephone()); %></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%
                        }

                    %>

                    <div class="col-sm-6 infosClient">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="text-center">
                                    <h2>Gestions Client</h2>
                                </div>
                                <hr>
                            </div>
                        </div>


                        <div class="spacer row">
                            <div class="text-center col-sm-6">
                                <div class="text-center">
                                    <%
                                        List<CompteClient> compte =(List<CompteClient>) request.getAttribute("comptes");
                                        if (compte != null) {
                                            for (CompteClient comptes : compte) {

                                    %>
                                    <button class="boutonInfos btn btn-primary">  <a href="CompteServlet?id=<%out.print(comptes.getIdrevenus());%>">Comptes</a></button>
                                    <%
                                                //  out.print("<a href=\"CompteServlet?id="+comptes.getIdrevenus() + "\" >Compte</a>");
                                            }
                                        }

                                    %>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="text-center">
                                    <button class="boutonInfos btn btn-primary"><a href="Credits/creditsClients.jsp">Crédits</a></button>
                                </div>
                            </div>
                        </div>

                        <div class="spacer row">
                            <div class="col-sm-6">
                                <div class="text-center">
                                    <button class="boutonInfos btn btn-primary"><a href="Assurances/assuranceClient.jsp">Assurance</a></button>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="text-center">
                                    <button class="boutonInfos btn btn-primary">Contacter</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

</div> <!-- /Wrapper -->





<!-- jQuery CDN - Slim version (=without AJAX) -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!-- Popper.JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<!-- Bootstrap JS -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });
</script>
</body>
</html>

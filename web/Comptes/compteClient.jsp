<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.nonlinebank.com.beans.CompteClient" %>


<%--
  Created by IntelliJ IDEA.
  User: cleme
  Date: 13/12/2018
  Time: 15:24
  To change this template use File | Settings | File Templates.
--%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Comptes Client</title>
    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="./css/style.css">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
</head>
<body>


<div class="wrapper">
    <!-- Sidebar -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3>N'Online Bank</h3>
            <strong>OB</strong>
        </div>
        <ul class="list-unstyled components">
            <li>
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-home"></i>
                    Accueil
                </a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li>
                        <a href="#">Tableau de bord</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#compteSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-briefcase"></i>
                    Comptes
                </a>
                <ul class="collapse list-unstyled" id="compteSubmenu">
                    <li>
                        <a href="#compteTypeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Gérer les comptes</a>
                        <ul class="collapse list-unstyled" id="compteTypeSubmenu">
                            <li>
                                <a href="#">&nbsp; Compte courant</a>
                            </li>
                            <li>
                                <a href="#">&nbsp; Livret A</a>
                            </li>
                            <li>
                                <a href="#">&nbsp; Livret jeune</a>
                            </li>
                            <hr class="solid">
                        </ul>
                    </li>
                    <li>
                        <a href="#">Opérations</a>
                    </li>
                    <li>
                        <a href="#">Détails</a>
                    </li>
                    <li>
                        <a href="#">Souscriptions</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#creditSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-copy"></i>
                    Crédits
                </a>
                <ul class="collapse list-unstyled" id="creditSubmenu">
                    <li>
                        <a href="#">Consommation</a>
                    </li>
                    <li>
                        <a href="#">Immobilier</a>
                    </li>
                    <li>
                        <a href="#">Automobile</a>
                    </li>
                    <li>
                        <a href="#">Etudiant</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#epargneSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-image"></i>
                    Epargne
                </a>
                <ul class="collapse list-unstyled" id="epargneSubmenu">
                    <li>
                        <a href="#">Plan logement</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#assuranceSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-paper-plane"></i>
                    Assurances
                </a>
                <ul class="collapse list-unstyled" id="assuranceSubmenu">
                    <li>
                        <a href="#">Assurance vie</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-question"></i>
                    FAQ
                </a>
            </li>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Déconnexion</a>
                    </li>
                </ul>
            </div>
        </ul>
    </nav>


    <!-- Page Content -->


    <div id="content">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-bars"></i>
                </button>
                <li class="nav-item list-unstyled">
                    <a class="nav-link" href="#" style="color: #17A2B8;"><i class="fas fa-search"></i>&nbsp; Recherche</a>
                </li>
                </button>
                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>
                <nav class="collapse navbar-collapse bg-light" aria-label="breadcrumb" id="navbarSupportedContent">
                    <ul class="breadcrumb ml-auto mb-auto text-info">
                        <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tableau de bord</li>
                    </ul>
                </nav>


            </div>
        </nav>





        <div class="container">



            <div class="container">
                <label>Client : Nathan Hego</label>
                <p>Gestion des comptes</p>
                <p>Numéro client : 15862545</p>
            </div>

            <div class = "container comptes">
                <p class="titreCompte">Compte Courant</p>

                <table class="spacer table table-striped table table-hover">

                    <thead>

                    <tr>
                        <th class="text-center">N° de compte</th>
                        <th class="text-center">Solde</th>
                        <th class="text-center">Découvert Autorisé</th>
                        <th class="text-center">Intérêts</th>
                    </tr>

                    </thead>

                    <tbody>

                    <%  List<CompteClient> comptes = (List<CompteClient>) request.getAttribute("comptes");

                        for (CompteClient compte : comptes) {

                    %>

                    <tr>
                        <td class="text-center">12355648</td>
                        <td class="text-center soldeCompte"><% out.println(compte.getMontant());%></td>
                        <td class="text-center">300€</td>
                        <td class="text-center">52%</td>

                        <%

                            }

                        %>
                    </tr>

                    </tbody>
                    <!-- Button trigger modal -->

                </table>
                <button type="button" class="boutonCompte btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Afficher plus
                </button>

            </div>

            <div class="container comptes">

                <p class="spacer titreCompte">Livret Jeune</p>

                <table class="spacer table table-striped table table-hover">

                    <thead>

                    <tr>

                        <th class="text-center">N° de compte</th>
                        <th class="text-center">Solde</th>
                        <th class="text-center">Découvert Autorisé</th>
                        <th class="text-center">Intérêts</th>

                    </tr>

                    </thead>

                    <tbody>

                    <tr>
                        <td class="text-center">12589648</td>
                        <td class="text-center soldeCompte">112.36</td>
                        <td class="text-center">100€</td>
                        <td class="text-center">2.7%</td>
                    </tr>
                    </tbody>

                </table>

                <button type="button" class="boutonCompte btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Afficher plus
                </button>

            </div>

            <div class="container comptes">

                <p class="spacer titreCompte">Livret A</p>

                <table class="spacer table table-striped table table-hover">

                    <thead>

                    <tr>

                        <th class="text-center">N° de compte</th>
                        <th class="text-center">Solde</th>
                        <th class="text-center">Découvert Autorisé</th>
                        <th class="text-center">Intérêts</th>

                    </tr>

                    </thead>

                    <tbody>

                    <tr>
                        <td class="text-center">781427</td>
                        <td class="text-center soldeCompte">54.85</td>
                        <td class="text-center">200€</td>
                        <td class="text-center">2.1%</td>
                    </tr>

                    </tbody>

                </table>

                <button type="button" class="boutonCompte btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Afficher plus
                </button>

            </div>

        </div>


    </div> <!-- /Wrapper -->

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Détails du compte</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <table class="table table-striped table table-hover">

                        <thead>

                        <tr>

                            <th>Date</th>
                            <th>Type</th>
                            <th>Nom</th>
                            <th>N° Facture</th>
                            <th>Montant</th>

                        </tr>

                        </thead>


                        <tbody>

                        <tr>
                            <td>02/12/2018</td>
                            <td>CB</td>
                            <td>SARL TABATIERE</td>
                            <td>168745</td>
                            <td>-10.00€</td>
                        </tr>

                        <tr>
                            <td>02/12/2018</td>
                            <td>CB</td>
                            <td>SARL SUBWAY</td>
                            <td>89247</td>
                            <td>-15.00€</td>
                        </tr>

                        <tr>
                            <td>04/12/2018</td>
                            <td>Virement</td>
                            <td>APPLE</td>
                            <td>1562345</td>
                            <td>+1600.12€</td>
                        </tr>

                        <tr>
                            <td>09/12/2018</td>
                            <td>Chèque</td>
                            <td>FNAC</td>
                            <td>946541</td>
                            <td>-35.00€</td>
                        </tr>

                        </tbody>

                    </table>

                </div>

                <div class="modal-footer">
                    <button type="button" class="boutonCompte btn btn-secondary" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- jQuery CDN - Slim version (=without AJAX) -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!-- Popper.JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<!-- Bootstrap JS -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });
</script>
</body>
</html>